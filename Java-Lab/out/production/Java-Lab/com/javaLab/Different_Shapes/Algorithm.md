# Different shapes

### Aim :
       Write a java application to draw several shapes in the window.
       
### Algorithm :
       
       Step 1:
       Start the process.
       
       Step 2:
       Create a class Shapes which extends form JFrame.
       
       Step 3:
       Create a constructor to initialize 
       
       Step 4:
       Get input from the user through readLine.
       
       Step 5:
       Using the subString function trim the string, and display it through throw statement
       
       Step 6:
       Stop the process.